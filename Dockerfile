FROM ubuntu:22.10

RUN apt update

RUN DEBIAN_FRONTEND=noninteractive TZ=Europe/Paris apt-get -y install tzdata

RUN apt install -y apache2 curl git php php-xml php-zip zip php-intl php-mbstring php-cli php-gd php-sqlite3

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php

RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -

RUN apt install -y nodejs

WORKDIR /var/www/html

RUN rm *

COPY . /var/www/html/

RUN composer install

RUN npm install

RUN npm run build

RUN bin/console doctrine:database:create
RUN bin/console doctrine:migrations:migrate -n

RUN bin/console doctrine:fixtures:load -n

RUN chmod 777 var/data.db

RUN rm /etc/apache2/sites-available/000-default.conf
COPY docker/symfony.conf /etc/apache2/sites-available/

RUN a2ensite symfony.conf
RUN a2enmod rewrite

EXPOSE 80
CMD apachectl -D FOREGROUND