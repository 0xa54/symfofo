<?php

namespace App\Security;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use App\Entity\User;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginFormAuthenticator extends AbstractLoginFormAuthenticator
{
	use TargetPathTrait;
	
	public const LOGIN_ROUTE = 'app_login';
	
	private UrlGeneratorInterface $urlGenerator;
	
	private UserRepository $userRepository;

	private TranslatorInterface $translator;
	
	public function __construct(UrlGeneratorInterface $urlGenerator, UserRepository $userRepository, TranslatorInterface $translator)
	{
		$this->urlGenerator = $urlGenerator;
		$this->userRepository = $userRepository;
		$this->translator = $translator;
	}
	
	public function authenticate(Request $request): Passport
	{
		$username = $request->request->get('username', '');
		$phrase = $request->getSession()->get('phrase');
		$captcha = $request->request->get('captcha', '');
		
		if ($phrase != $captcha) {
			throw new UserMessageAuthenticationException($this->translator->trans('wrong_captcha'));
		}
		
		/** @var User */
		$user = $this->userRepository->findOneBy(['email' => $username]);
		
		if (!$user) {
			throw new UserMessageAuthenticationException($this->translator->trans('user_not_found'));
		}

		if ($user->isLocked()) {
			throw new UserMessageAuthenticationException($this->translator->trans('user_locked'));
		}
		
		if (!$user->isEmailConfirmed()) {
			throw new UserMessageAuthenticationException($this->translator->trans('email_not_confirmed'));
		}
		
		$request->getSession()->set(Security::LAST_USERNAME, $username);
		
		return new Passport(new UserBadge($username),
		new PasswordCredentials($request->request->get('password', '')),
		[
			new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
			]
		);
	}
	
	public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
	{
		/** @var User */
		$user = $token->getUser();

		if ($user->getLocale()) {
			$request->getSession()->set('_locale', $user->getLocale());
		}

		if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
			return new RedirectResponse($targetPath);
		}
		
		// For example:
		return new RedirectResponse($this->urlGenerator->generate('app'));
	}
	
	public function supports(Request $request): bool
	{
		return $request->isMethod('POST') && $this->getLoginUrl($request) === $request->getRequestUri();
	}
	
	protected function getLoginUrl(Request $request): string
	{
		return $this->urlGenerator->generate(self::LOGIN_ROUTE);
	}
}
