<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserAdminType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('username', TextType::class, [
				'label' => 'username'
			])
			->add('email', EmailType::class, [
				'label' => 'email'
			])
			->add('locked', CheckboxType::class, [
				'label' => 'locked',
				'required' => false
			])
			->add('save', SubmitType::class, [
				'label' => 'save',
				'attr' => [
					'class' => 'ui button primary'
				]
			])
		;

		if($options['roles']) {
			$builder->add('roles', ChoiceType::class, [
				'label' => 'roles',
				'choices' => $options['roles']
			]);

			$builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($rolesArray) {
                     // transform the array to a string
                     return count($rolesArray)? $rolesArray[0]: null;
                },
                function ($rolesString) {
                     // transform the string back to an array
                     return [$rolesString];
                }
        	));
		}

		if($options['email_confirmation']) {
			$builder
				->add('emailConfirmed', CheckboxType::class, [
					'label' => 'email_confirmed',
					'required' => false
				])
			;
		}
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => User::class,
			'roles' => [],
			'email_confirmation' => false
		]);
	}
}