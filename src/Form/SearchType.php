<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('keywords', TextType::class, [
				'label' => 'keywords'
			])
			->add('submit', SubmitType::class, [
				'label' => 'search',
				'attr' => [
					'class' => 'ui button primary'
				]
			])
        ;

		$builder->get('keywords')
            ->addModelTransformer(new CallbackTransformer(
                function ($keywords) {
                     // transform the array to a string
                     return is_array($keywords) ? implode(' ', $keywords) : null;
                },
                function ($keywords) {
                     // transform the string back to an array
                     return explode(' ', $keywords);
                }
        ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
