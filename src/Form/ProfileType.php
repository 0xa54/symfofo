<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('signature', TextareaType::class, [
            'label' => 'signature'
        ])
        ->add('profile_picture', FileType::class, [
            'mapped' => false,
            'required' => false,
			'label' => 'profile_picture',
            'constraints' => [
                new Image([
                    'maxSize' => '1024k'
                ])
            ],
        ])
        ->add('delete_profile_picture', CheckboxType::class, [
            'label' => 'delete_profile_picture',
            'required' => false,
            'mapped' => false
        ])
        ->add('save', SubmitType::class, [
			'label' => 'save',
            'attr' => [
                'class' => 'ui button'
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
