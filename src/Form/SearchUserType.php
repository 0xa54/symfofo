<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('keywords', SearchType::class, [
				'label' => false,
				'required' => false,
				'attr' => [
					'placeholder' => 'username_or_email'
				]
			])
			->add('submit', SubmitType::class, [
				'label' => 'search',
				'row_attr' => [
					'class' => 'field'
				],
				'attr' => [
					'class' => 'ui button primary'
				]
			])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
