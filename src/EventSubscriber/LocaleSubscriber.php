<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleSubscriber implements EventSubscriberInterface
{
	private const _LOCALE = "_locale";
	private $defaultLocale;
	
	public function __construct(string $defaultLocale = 'en')
	{
		$this->defaultLocale = $defaultLocale;
	}
	
	public function onKernelRequest(RequestEvent $event)
	{
		$request = $event->getRequest();
		if (!$request->hasPreviousSession()) {
			return;
		}
		
		// try to see if the locale has been set as a _locale routing parameter
		if ($locale = $request->attributes->get(self::_LOCALE)) {
			$request->getSession()->set(self::_LOCALE, $locale);
		} else {
			// if no explicit locale has been set on this request, use one from the session
			$request->setLocale($request->getSession()->get(self::_LOCALE, $this->defaultLocale));
		}
	}
	
	public static function getSubscribedEvents(): array
	{
		return [
			// must be registered before (i.e. with a higher priority than) the default Locale listener
			KernelEvents::REQUEST => [['onKernelRequest', 20]],
		];
	}
}
