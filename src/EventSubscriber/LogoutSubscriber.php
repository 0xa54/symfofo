<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;

class LogoutSubscriber implements EventSubscriberInterface
{
	private $security;
	private $manager;

	public function __construct(Security $security, ManagerRegistry $managerRegistry)
	{
		$this->security = $security;
		$this->manager = $managerRegistry->getManager();
	}

	public function onLogout(LogoutEvent $logoutEvent): void
	{
		/** @var User */
		$user = $this->security->getUser();
		
		if($user) {
			$user->setLastActivityAt(null);
			$this->manager->flush();
		}
	}
	
	public static function getSubscribedEvents(): array
	{
		return [
			// must be registered before (i.e. with a higher priority than) the default Locale listener
			LogoutEvent::class => [['onLogout', 20]],
		];
	}
}