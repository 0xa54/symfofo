<?php

namespace App\EventSubscriber;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;

class ActivitySubscriber implements EventSubscriberInterface
{
	private $security;
	private $manager;

	public function __construct(Security $security, ManagerRegistry $managerRegistry)
	{
		$this->security = $security;
		$this->manager = $managerRegistry->getManager();
	}
	
	public function onTerminate()
	{
		/** @var User */
		$user = $this->security->getUser();
		
		if($user) {
			if (!$user->isActiveNow()) {
				$user->setLastActivityAt(new \DateTimeImmutable());
				$this->manager->flush($user);
			}
		}
	}
	
	public static function getSubscribedEvents(): array
	{
		return [
			// must be registered before (i.e. with a higher priority than) the default Locale listener
			KernelEvents::TERMINATE => [['onTerminate', 20]],
		];
	}
}
