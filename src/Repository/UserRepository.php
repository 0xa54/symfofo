<?php

namespace App\Repository;

use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
* @method User|null find($id, $lockMode = null, $lockVersion = null)
* @method User|null findOneBy(array $criteria, array $orderBy = null)
* @method User[]    findAll()
* @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
*/
class UserRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, User::class);
	}
	
	public function add(User $entity, bool $flush = false): void
	{
		$this->getEntityManager()->persist($entity);
		
		if ($flush) {
			$this->getEntityManager()->flush();
		}
	}
	
	public function remove(User $entity, bool $flush = false): void
	{
		$this->getEntityManager()->remove($entity);
		
		if ($flush) {
			$this->getEntityManager()->flush();
		}
	}
	
	/**
	* @return User[]
	*/
	public function findAllConnected()
	{
		return $this->createQueryBuilder('u')
		->select('u.username, u.roles, (JULIANDAY(:datetimeNow) - JULIANDAY(u.lastActivityAt)) * 24 * 60 as time')
		->where('(JULIANDAY(:datetimeNow) - JULIANDAY(u.lastActivityAt)) * 24 * 60 < 20')
		->setParameter('datetimeNow', new DateTime())
		->getQuery()->getResult();
	}
	
	/**
	* @return Paginator
	*/
	public function findUsers(int $page, int $pageSize = 10)
	{
		$firstResult = ($page - 1) * $pageSize;
		
		$queryBuilder = $this->createQueryBuilder('u');
		
		// Set the returned page
		$queryBuilder->setFirstResult($firstResult);
		$queryBuilder->setMaxResults($pageSize);
		
		// Generate the Query
		$query = $queryBuilder->getQuery();
		
		// Generate the Paginator
		return new Paginator($query, true);
	}
	
	/**
	* @return Paginator
	*/
	public function findUsersByUsernameOrEmail(?string $usernameOrEmail, int $page, int $pageSize = 10)
	{
		$firstResult = ($page - 1) * $pageSize;
		
		$queryBuilder = $this->createQueryBuilder('u');
		
		if($usernameOrEmail) {
			$queryBuilder
			->where('u.username LIKE :username')
			->orWhere('u.email LIKE :email')
			->setParameter('username', '%' . $usernameOrEmail . '%')
			->setParameter('email', '%' . $usernameOrEmail . '%');
		}
		
		// Set the returned page
		$queryBuilder->setFirstResult($firstResult);
		$queryBuilder->setMaxResults($pageSize);
		
		// Generate the Query
		$query = $queryBuilder->getQuery();
		
		// Generate the Paginator
		return new Paginator($query, true);
	}
}
