<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'],
message: 'email.already_exist',
)]
#[UniqueEntity(fields: ['username'],
message: 'username.already_exist',
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;
	
	#[ORM\Column(type: 'string', length: 180, unique: true)]
	#[Assert\NotBlank(message:"email.required")]
	#[Assert\Email(message:"email.invalid")]
	private $email;
	
	#[ORM\Column(type: 'json')]
	private $roles = [];
	
	#[ORM\Column(type: 'string')]
	#[Assert\NotBlank(message:'password.required')]
	#[Assert\Length(min: 8, minMessage: 'password.too_short', max: 64, maxMessage: 'password.too_long')]
	private $password;
	
	#[ORM\Column(type: 'string', length: 255)]
	#[Assert\NotBlank(message:'Username is not required')]
	#[Assert\Length(min: 3, minMessage: 'username.too_short', max: 12, maxMessage: 'username.too_long')]
	private $username;
	
	#[ORM\Column(type: 'string', length: 255, nullable: true)]
	private $emailConfirmationToken;
	
	#[ORM\Column(type: 'boolean')]
	private $emailConfirmed;
	
	#[ORM\Column(type: 'string', length: 255, nullable: true)]
	private $passwordResetToken;
	
	#[ORM\Column(type: 'datetime', nullable: true)]
	private $passwordResetTokenLast;
	
	#[ORM\Column(type: 'text', nullable: true)]
	private $signature;
	
	#[ORM\Column(type: 'string', length: 255, nullable: true)]
	private $profilePictureFileName;
	
	#[ORM\OneToMany(targetEntity:Topic::class, mappedBy:"author")]
	private $topics;
	
	#[ORM\OneToMany(targetEntity:Post::class, mappedBy:"author")]
	private $posts;
	
	#[ORM\Column(type: 'string', length: 255, nullable: true)]
	private $locale;
	
	#[ORM\Column(type: 'boolean', options: ['default' => 0])]
	private $locked;
	
	#[ORM\Column(type: 'datetime_immutable', nullable: true)]
	private $lastActivityAt;
	
	public function __construct()
	{
		$this->emailConfirmed = false;
		$this->locale = 'en';
		$this->roles = ['ROLE_USER'];
		$this->locked = false;
	}
	
	public function getId(): ?int
	{
		return $this->id;
	}
	
	public function getEmail(): ?string
	{
		return $this->email;
	}
	
	public function setEmail(string $email): self
	{
		$this->email = $email;
		
		return $this;
	}
	
	/**
	* A visual identifier that represents this user.
	*
	* @see UserInterface
	*/
	public function getUserIdentifier(): string
	{
		return (string) $this->email;
	}
	
	/**
	* @see UserInterface
	*/
	public function getRoles(): array
	{
		$roles = $this->roles;
		// guarantee every user at least has ROLE_USER
		$roles[] = 'ROLE_USER';
		
		return array_unique($roles);
	}
	
	public function setRoles(array $roles): self
	{
		$this->roles = $roles;
		
		return $this;
	}
	
	/**
	* @see PasswordAuthenticatedUserInterface
	*/
	public function getPassword(): string
	{
		return $this->password;
	}
	
	public function setPassword(string $password): self
	{
		$this->password = $password;
		
		return $this;
	}
	
	/**
	* @see UserInterface
	*/
	public function eraseCredentials()
	{
		// If you store any temporary, sensitive data on the user, clear it here
		// $this->plainPassword = null;
	}
	
	public function getUsername(): ?string
	{
		return $this->username;
	}
	
	public function setUsername(string $username): self
	{
		$this->username = $username;
		
		return $this;
	}
	
	public function getEmailConfirmationToken(): ?string
	{
		return $this->emailConfirmationToken;
	}
	
	public function setEmailConfirmationToken(?string $emailConfirmationToken): self
	{
		$this->emailConfirmationToken = $emailConfirmationToken;
		
		return $this;
	}
	
	public function isEmailConfirmed(): ?bool
	{
		return $this->emailConfirmed;
	}
	
	public function setEmailConfirmed(bool $emailConfirmed): self
	{
		$this->emailConfirmed = $emailConfirmed;
		
		return $this;
	}
	
	public function getPasswordResetToken(): ?string
	{
		return $this->passwordResetToken;
	}
	
	public function setPasswordResetToken(?string $passwordResetToken): self
	{
		$this->passwordResetToken = $passwordResetToken;
		
		return $this;
	}
	
	public function getPasswordResetTokenLast(): ?\DateTimeInterface
	{
		return $this->passwordResetTokenLast;
	}
	
	public function setPasswordResetTokenLast(?\DateTimeInterface $passwordResetTokenLast): self
	{
		$this->passwordResetTokenLast = $passwordResetTokenLast;
		
		return $this;
	}
	
	public function getSignature(): ?string
	{
		return $this->signature;
	}
	
	public function setSignature(?string $signature): self
	{
		$this->signature = $signature;
		
		return $this;
	}
	
	public function getProfilePictureFileName(): ?string
	{
		return $this->profilePictureFileName;
	}
	
	public function setProfilePictureFileName(?string $profilePictureFileName): self
	{
		$this->profilePictureFileName = $profilePictureFileName;
		
		return $this;
	}
	
	/**
	* @return Collection|Topic[]
	*/
	public function getTopics(): Collection
	{
		return $this->topics;
	}
	
	public function addTopic(Topic $topic): self
	{
		if (!$this->topics->contains($topic)) {
			$this->topics[] = $topic;
			$topic->setAuthor($this);
		}
		
		return $this;
	}
	
	public function removeTopic(Topic $topic): self
	{
		if ($this->topics->removeElement($topic) && $topic->getAuthor() === $this) {
			$topic->setAuthor(null);
		}
		
		return $this;
	}
	
	/**
	* @return Collection|Post[]
	*/
	public function getPosts(): Collection
	{
		return $this->posts;
	}
	
	public function addPost(Post $post): self
	{
		if (!$this->posts->contains($post)) {
			$this->posts[] = $post;
			$post->setAuthor($this);
		}
		
		return $this;
	}
	
	public function removePost(Post $post): self
	{
		if ($this->posts->removeElement($post) && $post->getAuthor() === $this) {
			$post->setAuthor(null);
		}
		
		return $this;
	}
	
	public function getLocale(): ?string
	{
		return $this->locale;
	}
	
	public function setLocale(?string $locale): self
	{
		$this->locale = $locale;
		
		return $this;
	}
	
	public function isLocked(): ?bool
	{
		return $this->locked;
	}
	
	public function setLocked(bool $locked): self
	{
		$this->locked = $locked;
		
		return $this;
	}
	
	/**
	* @return bool Whether the user is active or not
	*/
	public function isActiveNow(): bool
	{
		// Delay during wich the user will be considered as still active
		$delay = new \DateTime('20 minutes ago');
		return ($this->getLastActivityAt() > $delay);
	}
	
	public function getLastActivityAt(): ?\DateTimeImmutable
	{
		return $this->lastActivityAt;
	}
	
	public function setLastActivityAt(?\DateTimeImmutable $lastActivityAt): self
	{
		$this->lastActivityAt = $lastActivityAt;
		
		return $this;
	}
}
