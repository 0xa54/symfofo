<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[orm\HasLifecycleCallbacks]
#[ORM\InheritanceType("JOINED")]
#[ORM\DiscriminatorColumn(name: "discr", type: "string")]
#[ORM\DiscriminatorMap(['topic' => Topic::class, 'post' => Post::class])]
class Message
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	protected $id;
	
	#[ORM\Column(type: 'text')]
	protected $message;
	
	#[ORM\Column(type: 'datetime_immutable')]
	protected $createdAt;
	
	#[ORM\Column(type: 'datetime_immutable')]
	protected $updatedAt;
	
	public function getId(): ?int
	{
		return $this->id;
	}
	
	public function getMessage(): ?string
	{
		return $this->message;
	}
	
	public function setMessage(string $message): self
	{
		$this->message = $message;
		
		return $this;
	}
	
	public function getCreatedAt(): ?\DateTimeImmutable
	{
		return $this->createdAt;
	}
	
	public function setCreatedAt(\DateTimeImmutable $createdAt): self
	{
		$this->createdAt = $createdAt;
		
		return $this;
	}
	
	public function getUpdatedAt(): ?\DateTimeImmutable
	{
		return $this->updatedAt;
	}
	
	public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
	{
		$this->updatedAt = $updatedAt;
		
		return $this;
	}
	
	#[ORM\PrePersist]
	public function setCreatedAtValue()
	{
		$this->createdAt = new \DateTimeImmutable();
	}
	
	#[ORM\PrePersist]
	#[ORM\PreUpdate]
	public function setUpdatedAtValue()
	{
		$this->updatedAt = new \DateTimeImmutable();
	}
}
