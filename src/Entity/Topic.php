<?php

namespace App\Entity;

use App\Repository\TopicRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass:TopicRepository::class)]
class Topic extends Message
{
	#[ORM\ManyToOne(targetEntity:Category::class, inversedBy:"topics")]
	#[ORM\JoinColumn(nullable:false)]
	private $category;
	
	#[ORM\Column(type:'string', length:255)]
	private $title;
	
	#[ORM\OneToMany(targetEntity:Post::class, mappedBy:"topic", orphanRemoval:true, fetch:"EAGER")]
	#[ORM\OrderBy(['id' => 'DESC'])]
	private $posts;
	
	#[ORM\Column(type:'bigint')]
	private $views;
	
	#[ORM\ManyToOne(targetEntity:User::class, inversedBy:"topics")]
	private $author;
	
	#[ORM\Column(type: 'boolean', options: ['default' => false])]
	private $locked;
	
	public function __construct()
	{
		$this->posts = new ArrayCollection();
		$this->views = 0;
		$this->locked = false;
	}
	
	public function getCategory(): ?Category
	{
		return $this->category;
	}
	
	public function setCategory(?Category $category): self
	{
		$this->category = $category;
		
		return $this;
	}
	
	public function getTitle(): ?string
	{
		return $this->title;
	}
	
	public function setTitle(string $title): self
	{
		$this->title = $title;
		
		return $this;
	}
	
	/**
	* @return Collection|Post[]
	*/
	public function getPosts(): Collection
	{
		return $this->posts;
	}
	
	public function addPost(Post $post): self
	{
		if (!$this->posts->contains($post)) {
			$this->posts[] = $post;
			$post->setTopic($this);
		}
		
		return $this;
	}
	
	public function removePost(Post $post): self
	{
		if ($this->posts->removeElement($post)) {
			// set the owning side to null (unless already changed)
			if ($post->getTopic() === $this) {
				$post->setTopic(null);
			}
		}
		
		return $this;
	}
	
	public function increaseView(): void
	{
		$this->views++;
	}
	
	public function getViews(): ?string
	{
		return $this->views;
	}
	
	public function setViews(string $views): self
	{
		$this->views = $views;
		
		return $this;
	}
	
	public function getLastPost(): ?Post
	{
		return $this->posts->count() ? $this->posts->last() : null;
	}
	
	public function getAuthor(): ?User
	{
		return $this->author;
	}
	
	public function setAuthor(?User $author): self
	{
		$this->author = $author;
		
		return $this;
	}
	
	public function isLocked(): ?bool
	{
		return $this->locked;
	}
	
	public function setLocked(bool $locked): self
	{
		$this->locked = $locked;
		
		return $this;
	}
}
