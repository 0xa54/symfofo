<?php

namespace App\Entity;

use App\Repository\PostRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass:PostRepository::class)]
class Post extends Message
{    
    #[ORM\ManyToOne(targetEntity:Topic::class, inversedBy:"posts")]
    #[ORM\JoinColumn(nullable:false)]
    private $topic;

    #[ORM\ManyToOne(targetEntity:User::class, inversedBy:"posts")]
    private $author;
    
    public function getTopic(): ?Topic
    {
        return $this->topic;
    }
    
    public function setTopic(?Topic $topic): self
    {
        $this->topic = $topic;
        
        return $this;
    }
    
    public function getAuthor(): ?User
    {
        return $this->author;
    }
    
    public function setAuthor(?User $author): self
    {
        $this->author = $author;
        
        return $this;
    }
}
