<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

class LanguageController extends AbstractController
{
	#[Route('/language/{locale}', name: 'app_language', defaults:['locale' => 'en'])]
	public function index(Request $request, ManagerRegistry $managerRegistry, $locale = 'en'): Response
	{
		$locales = ['fr','en'];
		
		if (in_array($locale, $locales)) {
			$request->getSession()->set('_locale', $locale);
			
			/** @var User */
			$user = $this->getUser();
			
			if ($user) {
				$user->setLocale($locale);

				$manager = $managerRegistry->getManager();
				$manager->flush();
			}
		}
		
		$referer = $request->headers->get('referer');
		return new RedirectResponse($referer);
	}
}
