<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
	#[Route('/', name: 'app')]
	public function index(CategoryRepository $categoryRepository): Response
	{
		return $this->render('index.html.twig', [
			'categories' => $categoryRepository->findBy(['parent' => null]),
		]);
	}
	
	public function online(UserRepository $userRepository): Response
	{
		$users = $userRepository->findAllConnected();

		return $this->render('online.html.twig', [
			'users' => $users
		]);
	}
}
