<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\SearchUserType;
use App\Form\UserAdminType;
use App\Form\UserCreateAdminType;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminUserController extends AbstractController
{
	private $managerRegistry;
	
	public function __construct(ManagerRegistry $managerRegistry)
	{
		$this->managerRegistry = $managerRegistry;
	}
	
	#[Route('/admin/users', name: 'app_admin_user')]
	public function index(Request $request, UserRepository $userRepository): Response
	{
		$currentPage = $request->query->get('page', 1);
		$search = $request->query->get('search', null);
		
		$form = $this->createForm(SearchUserType::class);
		$form->handleRequest($request);
		
		if($form->isSubmitted() && $form->isValid())
		{
			$search = $form->get('keywords')->getData();			
		}
		
		$users = $userRepository->findUsersByUsernameOrEmail($search, $currentPage);
		
		return $this->render('admin/user/index.html.twig', [
			'users' => $users,
			'currentPage' => $currentPage,
			'form' => $form->createView(),
			'search' => $search
		]);
	}
	
	#[Route('/admin/users/{id}', name: 'app_admin_user_edit', requirements: ['id' => '\d+'])]
	public function edit(Request $request, User $user, TranslatorInterface $translator): Response
	{
		$allow = $this->isGranted('ROLE_MOD') && ($user->getRoles() == ['ROLE_USER'] || $user == $this->getUser())
		|| $this->isGranted('ROLE_ADMIN') && (($user->getRoles() == ['ROLE_USER'] || in_array('ROLE_MOD', $user->getRoles())) || $user == $this->getUser())
		|| $this->isGranted('ROLE_SUPER_ADMIN');
		
		if(!$allow) {
			return $this->redirectToRoute('app_admin_user');
		}
		
		$emailConfirmation = false;
		$roles = [];
		
		if($user != $this->getUser()) {
			$roles['User'] = 'ROLE_USER';

			if($this->isGranted('ROLE_ADMIN')) {
				$roles['Moderator'] = 'ROLE_MOD';
				$emailConfirmation = true;
			}
			
			if($this->isGranted('ROLE_SUPER_ADMIN')) {
				$roles += [
					'Admin' => 'ROLE_ADMIN',
					'Super-Admin' => 'ROLE_SUPER_ADMIN'
				];
			}
		} else {
			$emailConfirmation = true;
		}

		$form = $this->createForm(UserAdminType::class, $user, [
			'roles' => $roles,
			'email_confirmation' => $emailConfirmation
		]);
		$form->handleRequest($request);
		
		// Check if form is submitted and valid
		if ($form->isSubmitted() && $form->isValid()) {
			
			$this->managerRegistry->getManager()->flush();
			
			$this->addFlash('success', $translator->trans('informations_saved'));
		}
		
		return $this->render('admin/user/edit.html.twig', [
			'form' => $form->createView(),
			'user' => $user
		]);
	}
	
	#[Route('/admin/users/create', name: 'app_admin_user_create')]
	public function create(Request $request, UserPasswordHasherInterface $userPasswordHasherInterface, UserRepository $userRepository): Response
	{
		$roles = [
			'User' => 'ROLE_USER'
		];
		
		if($this->isGranted('ROLE_ADMIN')) {
			$roles['Moderator'] = 'ROLE_MOD';
		}
		
		if($this->isGranted('ROLE_SUPER_ADMIN')) {
			$roles['Admin'] = 'ROLE_ADMIN';
			$roles['Super-Admin'] = 'ROLE_SUPER_ADMIN';
		}
		
		$user = new User();
		
		$form = $this->createForm(UserCreateAdminType::class, $user, [
			'roles' => $roles
		]);
		$form->handleRequest($request);
		
		// Check if form is submitted and valid
		if ($form->isSubmitted() && $form->isValid()) {
			
			$user->setPassword($userPasswordHasherInterface->hashPassword($user, $form->get('password')->getData()));
			$user->setEmailConfirmed(true);
			
			$userRepository->add($user, true);
			
			return $this->redirectToRoute('app_admin_user');
		}
		
		return $this->render('admin/user/create.html.twig', [
			'form' => $form->createView()
		]);
	}
	
	#[Route('/admin/users/{id}/delete', name: 'app_admin_user_delete')]
	public function delete(User $user, UserRepository $userRepository): Response
	{
		$this->managerRegistry->getManager()->remove($user);
		$this->managerRegistry->getManager()->flush();
		
		$userRepository->remove($user, true);
		
		return $this->redirectToRoute('app_admin_user');
	}
}
