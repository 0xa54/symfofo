<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Repository\MessageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route('/search', name: 'app_search')]
    public function index(Request $request, MessageRepository $messageRepository): Response
    {
		$form = $this->createForm(SearchType::class);
		$form->handleRequest($request);

		$messages = new ArrayCollection();

		if ($form->isSubmitted() && $form->isValid())
		{
			$keywords = $form->get('keywords')->getData();
			$messages = $messageRepository->findBySearch($keywords);
		}

        return $this->render('search/index.html.twig', [
            'form' => $form->createView(),
			'messages' => $messages
        ]);
    }
}
