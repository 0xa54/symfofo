<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AccountControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');
		$userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('superadmin@local.host');

		$client->loginUser($testUser);

        $client->request('GET', $router->generate('app_account'));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $translator->trans('account'));
    }
}
