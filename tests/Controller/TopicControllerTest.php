<?php

namespace App\Tests\Controller;

use App\Repository\TopicRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Topic;

class TopicControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');

        /** @var TopicRepository */
		$topicRepository = static::getContainer()->get(TopicRepository::class);
		/** @var Topic */
		$topic = $topicRepository->findAll()[0];

		$client->request('GET', $router->generate('app_topic', [
			'id' => $topic->getId()
		]));

		$this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', $topic->getTitle());
    }
}
