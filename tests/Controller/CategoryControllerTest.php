<?php

namespace App\Tests\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Category;

class CategoryControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');

		/** @var CategoryRepository */
		$categoryRepository = static::getContainer()->get(CategoryRepository::class);
		/** @var Category */
		$category = $categoryRepository->findChildCategories()[0];

		$client->request('GET', $router->generate('app_category', [
			'id' => $category->getId()
		]));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $category->getName());
    }
}
