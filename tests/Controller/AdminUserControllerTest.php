<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminUserControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');
		$userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('superadmin@local.host');

		$client->loginUser($testUser);

        $client->request('GET', $router->generate('app_admin_user'));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $translator->trans('users'));
    }

	public function testCreate(): void
	{
		$client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');
		$userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('superadmin@local.host');

		$client->loginUser($testUser);

        $client->request('GET', $router->generate('app_admin_user_create'));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $translator->trans('create_user'));

		$client->submitForm($translator->trans('save'), [
            'user_create_admin[username]' => 'test',
			'user_create_admin[email]' => 'test@local.host',
			'user_create_admin[password]' => 'password',
			'user_create_admin[roles]' => 'ROLE_USER'
        ]);
		$client->followRedirect();

		$this->assertResponseIsSuccessful();
		$this->assertRouteSame('app_admin_user');
	}
}
