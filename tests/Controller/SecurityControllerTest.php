<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');

        $client->request('GET', $router->generate('app_login'));
        
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $translator->trans('login'));
    }
}
