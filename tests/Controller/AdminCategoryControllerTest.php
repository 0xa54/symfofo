<?php

namespace App\Tests\Controller;

use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminCategoryControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');
		$userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('superadmin@local.host');

		$client->loginUser($testUser);

        $client->request('GET', $router->generate('app_admin_category'));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $translator->trans('categories'));
    }

	public function testCreate(): void
	{
		$client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');
		$userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('superadmin@local.host');

		$client->loginUser($testUser);

        $client->request('GET', $router->generate('app_admin_category_create'));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $translator->trans('create_category'));

		$client->submitForm($translator->trans('save'), [
            'category_admin[name]' => 'Lorem ipsum',
			'category_admin[description]' => 'Lorem ipsum',
        ]);
	}

	public function testEdit(): void
	{
		$client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');
		$userRepository = static::getContainer()->get(UserRepository::class);

		$testUser = $userRepository->findOneByEmail('superadmin@local.host');

		$client->loginUser($testUser);

		/** @var CategoryRepository */
		$categoryRepository = static::getContainer()->get(CategoryRepository::class);
		/** @var Category */
		$category = $categoryRepository->findChildCategories()[0];

		$client->request('GET', $router->generate('app_admin_category_edit', [
			'id' => $category->getId()
		]));

		$this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $translator->trans('edit_category'));
	}
}
