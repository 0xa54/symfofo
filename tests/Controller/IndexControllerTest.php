<?php

namespace App\Tests\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');
		/** @var CategoryRepository */
		$categoryRepository = static::getContainer()->get(CategoryRepository::class);

        $crawler = $client->request('GET', $router->generate('app'));

		$categories = $categoryRepository->findBy([
			'parent' => null
		]);

		$this->assertResponseIsSuccessful();
        $this->assertCount(count($categories), $crawler->filter('.category'));
    }
}
