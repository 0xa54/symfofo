<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfileControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByUsername('admin');

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_profile'));

        $this->assertResponseIsSuccessful();
		$this->assertSelectorTextContains('h1', $translator->trans('profile'));

        $client->submitForm($translator->trans('save'), [
            'profile[signature]' => 'Lorem ipsum',
        ]);
    }
}
