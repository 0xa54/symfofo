<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SearchControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
		$router = static::getContainer()->get('router.default');
		$translator = static::getContainer()->get('translator');

        $client->request('GET', $router->generate('app_search'));

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $translator->trans('search'));
    }
}
