# Symfofo

Symfofo is a forum based on Symfony and Fomantic UI framework

**SonarQube**

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=Symfofo&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=Symfofo) [![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=Symfofo&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=Symfofo) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=Symfofo&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=Symfofo) [![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=Symfofo&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=Symfofo) [![Bugs](https://sonarcloud.io/api/project_badges/measure?project=Symfofo&metric=bugs)](https://sonarcloud.io/summary/new_code?id=Symfofo) [![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=Symfofo&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=Symfofo) [![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=Symfofo&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=Symfofo) [![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=Symfofo&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=Symfofo) 

**Gitlab CI**

![Pipeline Status](https://gitlab.com/0xa54/symfofo/badges/main/pipeline.svg)

**Docker**

[![Docker Pulls](https://badgen.net/docker/pulls/s0llvan/symfofo?icon=docker&label=pulls)](https://hub.docker.com/r/s0llvan/symfofo/)
[![Docker Stars](https://badgen.net/docker/stars/s0llvan/symfofo?icon=docker&label=stars)](https://hub.docker.com/r/s0llvan/symfofo/)
[![Docker Image Size](https://badgen.net/docker/size/s0llvan/symfofo?icon=docker&label=image%20size)](https://hub.docker.com/r/s0llvan/symfofo/)

## Preview

### Forum
![Forum](https://i.ibb.co/tDNHR3r/Screenshot-2022-12-18-at-20-45-06-Symfofo.png)

### Category
![Category](https://i.ibb.co/VScZvD6/Screenshot-2022-12-18-at-20-38-00-Symfofo-Aut-cum-aspernatur-optio.png)

### Topic
![Topic](https://i.ibb.co/Js8FhsR/Screenshot-2022-12-26-at-22-02-13-Symfofo-topic.png)

### Registration
![Registration](https://i.ibb.co/Fm7Lnhy/Screenshot-2022-12-18-at-20-37-43-Symfofo-Register.png)

## Requirements

- PHP >= 8.1
- NodeJS >= 18

## Installation

### Docker

```bash
docker run -d --name symfofo -p 80:80 s0llvan/symfofo
```

### Manual
Clone the repository
```bash
git clone https://github.com/s0llvan/Symfofo
```

Install dependencies
```bash
composer install
```

Install npm dependencies
```bash
npm install
```

Build assets
```bash
npm run build
```

Install database
```bash
bin/console doctrine:database:create
bin/console doctrine:migrations:migrate -n
```

Install fixtures (optionnal)
```bash
bin/console doctrine:fixtures:load -n
```

## Defaults access
User | Email | Password 
--- | --- | ---
superadmin | superadmin@local.host | password
admin | admin@local.host | password
mod | mod@local.host | password
user | user@local.host | password
